﻿Imports System
Imports System.Type
Imports System.Activator
Imports System.Runtime.InteropServices
Imports Inventor
Imports System.Math
Imports System.IO
Imports System.Text
Imports System.Windows.Forms
Imports Microsoft.VisualBasic.ErrObject



Public Class Form1

    Dim robotObj As New Robot
    Dim _invApp As Inventor.Application = Marshal.GetActiveObject("Inventor.Application")
    Dim defaultProjectPathString As String = "C:\Users\Tim\Documents\Visual Studio 2017\Projects\URDF Converter\URDF Converter\INV_TO_URDF\"
    Dim oUOM As UnitsOfMeasure = _invApp.ActiveDocument.UnitsOfMeasure
    Dim oAsmDoc As AssemblyDocument = _invApp.ActiveDocument
    Dim oAsmCompDef As AssemblyComponentDefinition = oAsmDoc.ComponentDefinition
    Dim materialList As New ArrayList
    Dim editFlag As Boolean = False
    Dim createFlag As Boolean = False

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Dim _started As Boolean = False
        Try
        Catch ex As Exception
            Try
                Dim invAppType As Type = GetTypeFromProgID("Inventor.Application")
                _invApp = CreateInstance(invAppType)
                _invApp.Visible = True
                _started = True

            Catch ex2 As Exception
                MsgBox(ex2.ToString())
                MsgBox("Unable to get or start Inventor")
            End Try
        End Try

        'setting the initial robot name to the name of the inventor assembly
        Dim robotName As String
        robotName = oAsmDoc.DisplayName.Replace(" ", "_")
        robotName = robotName.Replace(":", "_")
        If robotName.Contains(".iam") = True Then
            robotName = Strings.Left(robotName, InStr(robotName, ".iam") - 1)
        End If

        robotObj.Name = robotName
        'If robotName.Contains(".iam") Then
        'robotName.Remove(robotName.Length - 4)
        'End If       

        'setting the initial robot name textbox to the name of the inventor assembly
        robotNameTextBox.Text = robotObj.Name

        'setting the initial project path to the path of the current invnetor project
        Dim actProj As DesignProject = _invApp.DesignProjectManager.ActiveDesignProject
        Dim projPath As String = actProj.FullFileName.Substring(0, InStr(actProj.FullFileName, actProj.Name) - 1)
        ProjectDirectoryTextBox.Text = projPath

        'add initial package name = same as robot name
        ROSPackNameTextBox.Text = robotObj.Name

        'add an initial color gray 
        AddMaterial("gray", 104 / 255, 104 / 255, 104 / 255, 1)

        'setup material list from the material.txt file
        SetupMaterialList()

        'retrieve gray from list to assign to links and put the material in the combobox
        Dim grayMaterial As New LinkMaterial
        For Each mats As LinkMaterial In materialList
            If mats.Name = "gray" Then
                grayMaterial = mats
            End If
            MaterialComboBox.Items.Add(mats.Name)
            MaterialComboBox2.Items.Add(mats.Name)
        Next



        'setting up the link
        For Each oCompOccur As ComponentOccurrence In oAsmCompDef.Occurrences
            Call LinkSetup(oCompOccur, grayMaterial)
        Next

        'setting up the joints
        For Each asmJoint As AssemblyJoint In oAsmCompDef.Joints
            Call JointSetup(asmJoint)
        Next



        'this was used for getting information on the STL Exporter Add In
        'addInMacro()

        'this was used for writing a txt file with the link matrix to help debug matrix transformations
        'WriteDebugFile(robotObj)        


        writeCommandOutput()

    End Sub

    Private Sub LinkComboBox_SelectedIndexChanged(sender As Object, e As EventArgs) Handles LinkComboBox.SelectedIndexChanged

        Dim curLink As New Link

        'find the selected link in the link list
        For Each selLink As Link In robotObj.Links
            If selLink.Name = LinkComboBox.SelectedItem Then
                curLink = selLink
            End If
        Next

        LinkNameTextBox.Text = curLink.Name
        LinkMassTextBox.Text = curLink.mass
        MaterialComboBox.SelectedItem = curLink.Material.Name

    End Sub

    Private Sub CreateButton_Click(sender As Object, e As EventArgs) Handles createButton.Click

        'change the robot name to what was entered in the textbox
        robotObj.Name = robotNameTextBox.Text

        'get a list of materials used and add it to the robot material used list    
        Dim materialUsed As LinkMaterial
        Dim usedList As New ArrayList

        For Each link As Link In robotObj.Links
            materialUsed = link.Material
            If usedList.Contains(materialUsed) = False Then
                usedList.Add(materialUsed)
            End If
        Next

        robotObj.MaterialsUsed = usedList


        'write the urdf file
        Dim urdfWriter As New URDF_File_Writer
        Call urdfWriter.WriteFile(getProjectPath(), ROSPackNameTextBox.Text, robotObj)

        'export part files to STL files
        Dim oApprentice As New ApprenticeServerComponent
        Dim invDocs As Documents = _invApp.Documents

        For Each curComp As ComponentOccurrence In oAsmCompDef.Occurrences
            Dim curDoc As Document
            curDoc = invDocs.Open(curComp.ReferencedDocumentDescriptor.FullDocumentName, False)
            StlExporter(defaultProjectPathString, curDoc)
            curDoc.Close()
        Next

        Form1.ActiveForm.Close()

    End Sub

    Private Sub ProjectDirectoryBrowseButton_Click(sender As Object, e As EventArgs) Handles ProjectDirectoryBrowseButton.Click
        Dim folderBrowserDialog1 As New FolderBrowserDialog

        Dim result As DialogResult = folderBrowserDialog1.ShowDialog()

        Dim folderName As String

        If result = DialogResult.OK Then

            folderName = folderBrowserDialog1.SelectedPath
            ProjectDirectoryTextBox.Text = folderName

        ElseIf result = DialogResult.Cancel Then

            Return

        End If

    End Sub

    Private Sub JointComboBox_SelectedIndexChanged(sender As Object, e As EventArgs) Handles JointComboBox.SelectedIndexChanged
        Dim selectedJoint As New AsmJoint

        'find the selected joint in the robot joint list
        For Each selJoint As AsmJoint In robotObj.Joints
            If selJoint.Name = JointComboBox.SelectedItem Then
                selectedJoint = selJoint
            End If
        Next

        SetJointTabProperties(selectedJoint)

    End Sub

    Private Sub LinkApplyButton_Click(sender As Object, e As EventArgs) Handles LinkApplyButton.Click

        Dim selectedLink As New Link

        For Each link As Link In robotObj.Links
            If link.Name = LinkComboBox.SelectedItem Then
                selectedLink = link
            End If
        Next

        selectedLink.Name = LinkNameTextBox.Text

        Try
            selectedLink.mass = Convert.ToDouble(LinkMassTextBox.Text)
        Catch ex As Exception

        End Try

        For Each material As LinkMaterial In materialList
            If material.Name = MaterialComboBox.SelectedItem Then
                selectedLink.Material = material
            End If
        Next

        LinkComboBox.Items.Clear()
        SetLinkTabProperties(selectedLink, robotObj)

    End Sub

    Private Sub LinkCancelButton_Click(sender As Object, e As EventArgs) Handles LinkCancelButton.Click

        For Each link As Link In robotObj.Links
            If link.Name = LinkComboBox.SelectedItem Then
                SetLinkTabProperties(link, robotObj)
            End If
        Next

    End Sub

    Private Sub JointApplyButton_Click(sender As Object, e As EventArgs) Handles JointApplyButton.Click
        Dim selectedJoint As New AsmJoint

        For Each joint As AsmJoint In robotObj.Joints
            If joint.Name = JointComboBox.SelectedItem Then
                selectedJoint = joint
            End If
        Next

        If selectedJoint.Type = "revolute" Then
            Try
                selectedJoint.UpperLimit = Convert.ToDouble(UpperLimitTextBox.Text)
            Catch ex As Exception

            End Try

            Try
                selectedJoint.LowerLimit = Convert.ToDouble(LowerLimitTextBox.Text)
            Catch ex As Exception

            End Try
            'if the joint was continuous but contains limits, it will be changed to a revolute
        ElseIf selectedJoint.Type = "continuous" Then
            If UpperLimitTextBox.Text.Length > 0 Or LowerLimitTextBox.Text.Length > 0 Then
                selectedJoint.Type = "revolute"
                selectedJoint.IsReady = False   'do this in the case that not both limits were entered

                'have to separate these so if only one is set, it doesn't run the set property of the other one
                'which would automatically set the isReady property
                If UpperLimitTextBox.Text.Length > 0 Then
                    selectedJoint.UpperLimit = Convert.ToDouble(UpperLimitTextBox.Text)
                End If
                If LowerLimitTextBox.Text.Length > 0 Then
                    selectedJoint.LowerLimit = Convert.ToDouble(LowerLimitTextBox.Text)
                End If
            End If
        End If

        'store the values for the effort and velocity limits in the joint
        selectedJoint.EffortLimit = Convert.ToDouble(EffortLimitTextBox.Text)
        selectedJoint.VelocityLimit = Convert.ToDouble(VelocityLimitTextBox.Text)

        'reset the joint tab with the current values
        SetJointTabProperties(selectedJoint)

    End Sub

    Private Sub JointCancelButton_Click(sender As Object, e As EventArgs) Handles JointCancelButton.Click
        For Each joint As AsmJoint In robotObj.Joints
            If joint.Name = JointComboBox.SelectedItem Then
                SetJointTabProperties(joint)
            End If
        Next
    End Sub

    Private Sub MaterialCreateButton_Click(sender As Object, e As EventArgs) Handles MaterialCreateButton.Click

        createFlag = True
        ColorRectangle.BackColor = System.Drawing.Color.FromArgb(255, 104, 104, 104)
        MaterialNameTextBox.Text = ""
        ColorDialog1.ShowDialog()
        ColorRectangle.BackColor = ColorDialog1.Color

    End Sub

    Private Sub MaterialApplyButton_Click(sender As Object, e As EventArgs) Handles MaterialApplyButton.Click
        If createFlag = True Then
            createFlag = False
            'add color to the material list
            AddMaterial(MaterialNameTextBox.Text, ColorDialog1.Color.R / 255, ColorDialog1.Color.G / 255, ColorDialog1.Color.B / 255, ColorDialog1.Color.A / 255)

            'add this color to the link material combobox
            MaterialComboBox.Items.Add(MaterialNameTextBox.Text)
            MaterialComboBox2.Items.Add(MaterialNameTextBox.Text)

            'set the color rectangle on the material tab
            'ColorRectangle.BackColor = ColorDialog1.Color

            MaterialComboBox2.SelectedItem = MaterialNameTextBox.Text

        ElseIf editFlag = True Then
            editFlag = False
            Dim curMat As New LinkMaterial

            For Each mat As LinkMaterial In materialList
                If mat.Name = MaterialComboBox2.SelectedItem Then
                    mat.Red = ColorDialog1.Color.R
                    mat.Green = ColorDialog1.Color.G
                    mat.Blue = ColorDialog1.Color.B
                End If
            Next

        End If





    End Sub

    Private Sub MaterialComboBox2_SelectedIndexChanged(sender As Object, e As EventArgs) Handles MaterialComboBox2.SelectedIndexChanged
        Dim curMat As New LinkMaterial

        For Each mat As LinkMaterial In materialList
            If mat.Name = MaterialComboBox2.SelectedItem Then
                curMat = mat
            End If
        Next

        MaterialNameTextBox.Text = curMat.Name

        Dim colorObj As System.Drawing.Color
        colorObj = System.Drawing.Color.FromArgb(curMat.Alpha * 255, curMat.Red * 255, curMat.Green * 255, curMat.Blue * 255)

        ColorRectangle.BackColor = colorObj

    End Sub

    Private Sub MaterialEditButton_Click(sender As Object, e As EventArgs) Handles MaterialEditButton.Click

        editFlag = True
        ColorDialog1.ShowDialog()
        ColorRectangle.BackColor = ColorDialog1.Color

    End Sub

End Class
