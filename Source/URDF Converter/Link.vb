﻿Imports Inventor
Public Class Link
    Dim origXYZ As ArrayList
    Dim origRPY As ArrayList
    Dim lName As String
    Dim lMass As Double
    Dim cenOfMass As ArrayList
    Dim momOfIner As ArrayList
    Dim tranMatrix As Matrix
    Dim coOccur As ComponentOccurrence
    Dim fName As String
    Dim mater As LinkMaterial


    Property Name As String
        Get
            Return lName
        End Get
        Set(value As String)
            lName = value
        End Set
    End Property

    Property originXYZ As ArrayList
        Get
            Return origXYZ
        End Get
        Set(value As ArrayList)
            origXYZ = value
        End Set
    End Property

    Property originRPY As ArrayList
        Get
            Return origRPY
        End Get
        Set(value As ArrayList)
            origRPY = value
        End Set
    End Property

    Property mass As Double
        Get
            Return lMass
        End Get
        Set(value As Double)
            lMass = value
        End Set
    End Property

    Property centerOfMass As ArrayList
        Get
            Return cenOfMass
        End Get
        Set(value As ArrayList)
            cenOfMass = value
        End Set
    End Property

    Property momentOfInertia As ArrayList
        Get
            Return momOfIner
        End Get
        Set(value As ArrayList)
            momOfIner = value
        End Set
    End Property

    Property transMatrix As Matrix
        Get
            Return tranMatrix
        End Get
        Set(value As Matrix)
            tranMatrix = value
        End Set
    End Property

    Property compOccur As ComponentOccurrence
        Get
            Return coOccur
        End Get
        Set(value As ComponentOccurrence)
            coOccur = value
        End Set
    End Property

    Property fileName As String
        Get
            Return fName
        End Get
        Set(value As String)
            fName = value
        End Set
    End Property

    Property Material As LinkMaterial
        Get
            Return mater
        End Get
        Set(value As LinkMaterial)
            mater = value
        End Set
    End Property

End Class
