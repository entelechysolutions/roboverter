﻿Imports Inventor
Public Class AsmJoint
    Public jointName As String
    Public jointparentName As String
    Public jointchildName As String
    Public jointDefinition As AssemblyJointDefinition
    Public oJointObject As AssemblyJoint
    Public origXYZ As ArrayList
    Public origRPY As ArrayList
    Public jType As String
    Public lLimit As Double
    Public uLimit As Double
    Dim readyFlag As Boolean = True
    Dim uLimitReady As Boolean = False
    Dim lLimitReady As Boolean = False
    Dim vlimit As Double
    Dim elimit As Double

    Property Name() As String
        Get
            Return jointName
        End Get
        Set(value As String)
            jointName = value
        End Set
    End Property

    Property parentName As String
        Get
            Return jointparentName
        End Get
        Set(value As String)
            jointparentName = value
        End Set
    End Property

    Property childName As String
        Get
            Return jointchildName
        End Get
        Set(value As String)
            jointchildName = value
        End Set
    End Property

    Property definition As AssemblyJointDefinition
        Get
            Return jointDefinition
        End Get
        Set(value As AssemblyJointDefinition)
            jointDefinition = value
        End Set
    End Property

    Property jointObject As AssemblyJoint
        Get
            Return oJointObject
        End Get
        Set(value As AssemblyJoint)
            oJointObject = value
        End Set
    End Property

    Property originXYZ As ArrayList
        Get
            Return origXYZ
        End Get
        Set(value As ArrayList)
            origXYZ = value
        End Set
    End Property

    Property originRPY As ArrayList
        Get
            Return origRPY
        End Get
        Set(value As ArrayList)
            origRPY = value
        End Set
    End Property

    Property Type As String
        Get
            Return jType
        End Get
        Set(value As String)
            jType = value
        End Set
    End Property

    Property LowerLimit As Double
        Get
            Return lLimit
        End Get
        Set(value As Double)
            lLimit = value
            lLimitReady = True
            IsReadyCheck(uLimitReady, lLimitReady) 'check to see if both limits have been set
        End Set
    End Property

    Property UpperLimit As Double
        Get
            Return uLimit
        End Get
        Set(value As Double)
            uLimit = value
            uLimitReady = True
            IsReadyCheck(uLimitReady, lLimitReady) 'check to see if both limits have been set
        End Set
    End Property
    'this flag makes sure revolute joints have their limits set.  
    Property IsReady As Boolean
        Get
            Return readyFlag
        End Get
        Set(value As Boolean)
            readyFlag = value
        End Set
    End Property

    Private Function IsReadyCheck(uLim As Boolean, lLim As Boolean) As Object
        If lLim = True And uLim = True Then
            readyFlag = True
        End If
        Return 0
    End Function

    Property VelocityLimit As Double
        Get
            Return vlimit
        End Get
        Set(value As Double)
            vlimit = value
        End Set
    End Property

    Property EffortLimit As Double
        Get
            Return elimit
        End Get
        Set(value As Double)
            elimit = value
        End Set
    End Property

End Class

