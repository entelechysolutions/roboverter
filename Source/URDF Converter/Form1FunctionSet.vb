﻿Imports System
Imports System.Type
Imports System.Activator
Imports System.Runtime.InteropServices
Imports Inventor
Imports System.Math
Imports System.IO
Imports System.Text
Imports System.Windows.Forms
Imports Microsoft.VisualBasic.ErrObject


Partial Class Form1


    Private Function addTabs(inString As String, tabCount As Integer) As String
        Dim outString As String = ""
        Dim x As Integer = 0

        If tabCount = 0 Then
            outString = inString
            Return outString
        End If

        While x < tabCount
            outString = outString & "   "
            x += 1
        End While

        outString = outString & inString

        Return outString

    End Function

    Private Function SetLinkTabProperties(curLink As Link, robot As Robot) As Object
        LinkComboBox.Items.Clear()

        For Each link As Link In robot.Links
            LinkComboBox.Items.Add(link.Name)
        Next


        'insert link properties in respective locations
        LinkNameTextBox.Text = curLink.Name
        LinkMassTextBox.Text = curLink.mass
        MaterialComboBox.SelectedItem = curLink.Material.Name

        Return 0
    End Function

    Private Function SetupMaterialList() As Object
        'open the material database file
        Dim matFile As StreamReader
        Dim filePath As String = System.Environment.GetFolderPath(System.Environment.SpecialFolder.ApplicationData)
        filePath = filePath & "\AutoDesk\ApplicationPlugins\RoboVerter.bundle\Contents\Resources\material.txt"
        matFile = My.Computer.FileSystem.OpenTextFileReader(filePath)

        Dim currentLine As String = " "

        OutPutLine("setting up material list")

        Dim newMat As New LinkMaterial

        'keep reading the file until the END tag is read
        While currentLine.Equals("<END>") = False
            currentLine = matFile.ReadLine

            'if the current line has the material tag store the current and start a new material 
            If currentLine = "<material>" Then
                OutPutLine("new material")
                Dim curMaterial As New LinkMaterial
                Dim r, g, b, a As Integer
                Dim matName As String = ""

                'keep reading until the end of the current material is read
                While currentLine.Equals("</material>") = False
                    currentLine = matFile.ReadLine

                    If currentLine.Contains("<name") = True Then
                        Dim strLength As Integer = currentLine.Length
                        matName = Strings.Right(Strings.Left(currentLine, strLength - 2), strLength - 8)
                        OutPutLine("Name = " & matName)

                    ElseIf currentLine.Contains("<color") = True Then
                        Dim colorLineLength As Integer = currentLine.Length
                        Dim colorString As String = Strings.Right(currentLine, colorLineLength - 7)

                        r = Convert.ToInt32(Strings.Left(colorString, InStr(colorString, ",") - 1))
                        colorString = Strings.Right(colorString, colorString.Length - InStr(colorString, ","))

                        g = Convert.ToInt32(Strings.Left(colorString, InStr(colorString, ",") - 1))
                        colorString = Strings.Right(colorString, colorString.Length - InStr(colorString, ","))

                        b = Convert.ToInt32(Strings.Left(colorString, InStr(colorString, ",") - 1))
                        colorString = Strings.Right(colorString, colorString.Length - InStr(colorString, ","))

                        a = Convert.ToInt32(Strings.Left(colorString, InStr(colorString, "/") - 1))
                        OutPutLine("color = " & r.ToString & " " & g.ToString & " " & b.ToString)

                    End If
                End While

                'URDF rgba values are between 0 and 1
                AddMaterial(matName, r / 255, g / 255, b / 255, a / 255)

            End If


        End While

        matFile.Close()

        Return 0
    End Function

    Private Function OutPutLine(value As String) As Object
        System.Diagnostics.Debug.WriteLine(value)
        Return 0
    End Function

    Private Function WriteDebugFile(robotObj As Robot) As Object

        Dim outFile As String = "C:\Users\Tim\Documents\Visual Studio 2017\Projects\URDF Converter\URDF Converter\outputFile.txt"
        If My.Computer.FileSystem.FileExists(outFile) Then
            My.Computer.FileSystem.DeleteFile(outFile)
        End If

        Dim outFileSW As StreamWriter

        outFileSW = My.Computer.FileSystem.OpenTextFileWriter(outFile, True)
        outFileSW.AutoFlush = True

        For Each link As Link In robotObj.Links

            Dim curMatrix As Matrix = link.transMatrix
            Dim curLinkName As String = link.Name

            outFileSW.WriteLine(curLinkName)
            outFileSW.WriteLine(" ")
            outFileSW.WriteLine(curMatrix.Cell(1, 1) & "  " & curMatrix.Cell(1, 2) & "  " & curMatrix.Cell(1, 3) & "  " & curMatrix.Cell(1, 4))
            outFileSW.WriteLine(" ")
            outFileSW.WriteLine(curMatrix.Cell(2, 1) & "  " & curMatrix.Cell(2, 2) & "  " & curMatrix.Cell(2, 3) & "  " & curMatrix.Cell(2, 4))
            outFileSW.WriteLine(" ")
            outFileSW.WriteLine(curMatrix.Cell(3, 1) & "  " & curMatrix.Cell(3, 2) & "  " & curMatrix.Cell(3, 3) & "  " & curMatrix.Cell(3, 4))
            outFileSW.WriteLine(" ")
            outFileSW.WriteLine(curMatrix.Cell(4, 1) & "  " & curMatrix.Cell(4, 2) & "  " & curMatrix.Cell(4, 3) & "  " & curMatrix.Cell(4, 4))

            outFileSW.WriteLine(" ")
            outFileSW.WriteLine(" ")
            outFileSW.WriteLine(" ")
            outFileSW.WriteLine(" ")

        Next

        outFileSW.Close()


        Return 0
    End Function

    Private Function SetJointTabProperties(curJoint As AsmJoint) As Object

        'insert joint properties in respective locations
        jointChildLabel.Text = curJoint.childName
        jointParentLabel.Text = curJoint.parentName
        JointTypeLabel.Text = curJoint.Type
        EffortLimitTextBox.Text = curJoint.EffortLimit
        VelocityLimitTextBox.Text = curJoint.VelocityLimit
        If curJoint.Type = "revolute" Then
            UpperLimitTextBox.Text = curJoint.UpperLimit
            LowerLimitTextBox.Text = curJoint.LowerLimit
        Else
            UpperLimitTextBox.Clear()
            LowerLimitTextBox.Clear()
        End If

        If curJoint.IsReady = False Then
            UpperLimitTextBox.BackColor = Drawing.Color.Red
            LowerLimitTextBox.BackColor = Drawing.Color.Red
        Else
            UpperLimitTextBox.BackColor = Drawing.Color.Green
            LowerLimitTextBox.BackColor = Drawing.Color.Green
        End If



        Return 0
    End Function

    Private Function AddMaterial(mName As String, red As Double, green As Double, blue As Double, alpha As Double) As Object
        Dim newMaterial As New LinkMaterial
        Dim rgbVal As New ArrayList

        newMaterial.Name = mName

        newMaterial.Red = red
        newMaterial.Green = green
        newMaterial.Blue = blue
        newMaterial.Alpha = alpha

        materialList.Add(newMaterial)

        Return 0
    End Function

    Private Function addInMacro() As Object

        Dim oAddIns As ApplicationAddIns = _invApp.ApplicationAddIns

        'this is just a temporry permanant path
        Dim path As String = "C:\Users\Tim\Documents\Visual Studio 2017\Projects\URDF Converter\URDF Converter\"

        'this is just a temporary permanant file name
        Dim documentName As String = "Inventor_addin_report.txt"

        'the path of the file to be created is the path + the document name
        Dim fileName As String = path & documentName

        'check and see if the file already exists
        If My.Computer.FileSystem.FileExists(fileName) = True Then
            System.Diagnostics.Debug.WriteLine("FILE ALREADY EXISTED!!!!")
            My.Computer.FileSystem.DeleteFile(fileName)
        End If


        'this is opening the specified file for editing
        Dim fileSW As System.IO.StreamWriter
        fileSW = My.Computer.FileSystem.OpenTextFileWriter(fileName, True)

        Dim i As Long
        For i = 1 To oAddIns.Count

            On Error Resume Next
            Dim Addin As ApplicationAddIn
            Dim tranAddin As TranslatorAddIn

            Addin = oAddIns.Item(i)

            Dim aiType, aiID, aiName, aiOp As String

            aiType = Addin.Type.ToString
            aiID = Addin.ClientId
            aiName = Addin.DisplayName


            System.Diagnostics.Debug.WriteLine(" ")
            System.Diagnostics.Debug.WriteLine(aiName)
            System.Diagnostics.Debug.WriteLine(aiType)
            System.Diagnostics.Debug.WriteLine(aiID)
            System.Diagnostics.Debug.WriteLine(" ")

            System.Diagnostics.Debug.WriteLine(" ")
            System.Diagnostics.Debug.WriteLine(" ")

        Next

        Return 0

    End Function

    Private Function getProjectPath() As String
        Dim fPath As String
        fPath = ProjectDirectoryTextBox.Text & "\"

        Return fPath
    End Function

    Private Function StlExporter(path As String, curDoc As Document) As Object
        Dim tranAddIn As TranslatorAddIn = _invApp.ApplicationAddIns.ItemById("{533E9A98-FC3B-11D4-8E7E-0010B541CD80}")

        Dim oContext As TranslationContext = _invApp.TransientObjects.CreateTranslationContext
        Dim oOptions As NameValueMap = _invApp.TransientObjects.CreateNameValueMap


        'Dim docName As String = compOccur.ReferencedDocumentDescriptor.FullDocumentName
        Dim fileName, fileString As String


        If InStr(curDoc.DisplayName, ".") > 0 Then
            fileName = curDoc.DisplayName.Substring(0, InStr(curDoc.DisplayName, ".") - 1)
        Else
            fileName = curDoc.DisplayName
        End If

        fileString = getProjectPath() & "meshes\" & fileName.Replace(" ", "_") & ".stl"

        Dim oData As DataMedium = _invApp.TransientObjects.CreateDataMedium
        oData.FileName = fileString

        oContext.Type = IOMechanismEnum.kFileBrowseIOMechanism

        oOptions.Value("ExportFileStructure") = 0

        '4 = cm
        '5 = mm
        '6 = m
        oOptions.Value("ExportUnits") = 6

        'System.Diagnostics.Debug.WriteLine(compOccur.ReferencedDocumentDescriptor.FullDocumentName)

        'Dim oDoc As ApprenticeServerDocument = oApprentice.Open(compOccur.ReferencedDocumentDescriptor.FullDocumentName)

        Call tranAddIn.SaveCopyAs(curDoc, oContext, oOptions, oData)

        Return 0

    End Function

    Private Function LinkRPY(linkMat As Matrix) As ArrayList

        Dim roll As Double
        Dim pitch As Double
        Dim yaw As Double

        roll = Math.Atan2(linkMat.Cell(3, 2), linkMat.Cell(3, 3))
        pitch = Math.Atan2(-(linkMat.Cell(3, 1)), Math.Sqrt(linkMat.Cell(3, 2) * linkMat.Cell(3, 2) + linkMat.Cell(3, 3) * linkMat.Cell(3, 3)))
        yaw = Math.Atan2(linkMat.Cell(2, 1), linkMat.Cell(1, 1))

        Dim RPY As New ArrayList

        RPY.Add(roll)
        RPY.Add(pitch)
        RPY.Add(yaw)

        Return RPY

    End Function

    Private Function MatrixBaseChange(cMatrix As Matrix, pMatrix As Matrix, pName As String) As Collection
        Dim rpy As New ArrayList
        Dim xyz As New ArrayList
        Dim retCol As Collection = New Collection
        Dim vu As Matrix

        Dim r21, r11, r31, r32, r33, r14, r24, r34 As Double

        vu = pMatrix.Copy
        vu.Invert()
        'System.Diagnostics.Debug.WriteLine(" ")
        'System.Diagnostics.Debug.WriteLine(pName)
        'System.Diagnostics.Debug.WriteLine(vu.Cell(1, 1) & "  " & vu.Cell(1, 2))
        vu.PostMultiplyBy(cMatrix)

        r21 = vu.Cell(2, 1)
        r11 = vu.Cell(1, 1)
        r31 = vu.Cell(3, 1)
        r32 = vu.Cell(3, 2)
        r33 = vu.Cell(3, 3)
        r14 = vu.Cell(1, 4)
        r24 = vu.Cell(2, 4)
        r34 = vu.Cell(3, 4)

        Dim sRoot As Double = Math.Sqrt(Math.Pow(r32, 2) + Math.Pow(r33, 2))

        rpy.Add(Math.Atan2(r32, r33))
        rpy.Add(Math.Atan2(-(r31), sRoot))
        rpy.Add(Math.Atan2(r21, r11))

        xyz.Add(r14 / 100)
        xyz.Add(r24 / 100)
        xyz.Add(r34 / 100)

        retCol.Add(rpy)
        retCol.Add(xyz)

        Return retCol


    End Function

    Private Function LinkSetup(oCompOccur As ComponentOccurrence, grayMaterial As LinkMaterial) As Object
        Dim curLink As New Link

        'assign name to link, replace spaces with _ and : with _
        Dim linkName As String = oCompOccur.Name.ToString.Replace(" ", "_")
        curLink.Name = linkName.Replace(":", "_")
        OutPutLine(linkName)

        'store the links transformation matrix
        curLink.transMatrix = oCompOccur.Transformation

        'set the links initial material to gray
        curLink.Material = grayMaterial

        'get the current links mass
        Dim massProp As MassProperties = oCompOccur.MassProperties

        Dim mass As Double
        mass = massProp.Mass

        curLink.mass = mass

        'get the current links center of mass
        Dim cenOfMass As Point
        Dim inertOrig As New ArrayList

        cenOfMass = massProp.CenterOfMass()

        inertOrig.Add(Math.Round(cenOfMass.X / 100, 4))
        inertOrig.Add(Math.Round(cenOfMass.Y / 100, 4))
        inertOrig.Add(Math.Round(cenOfMass.Z / 100, 4))

        curLink.centerOfMass = inertOrig

        'get the current links inertia properties
        Dim Ixx As Double
        Dim Ixy As Double
        Dim Ixz As Double
        Dim Iyy As Double
        Dim Iyz As Double
        Dim Izz As Double

        Call massProp.XYZMomentsOfInertia(Ixx, Iyy, Izz, Ixy, Iyz, Ixz)

        'defaults to kg*cm^2 and we need kg*m^2
        Dim momOfInert As New ArrayList From {
                Math.Round(Ixx / 10000, 10),
                Math.Round(Ixy / 10000, 10),
                Math.Round(Ixz / 10000, 10),
                Math.Round(Iyy / 10000, 10),
                Math.Round(Iyz / 10000, 10),
                Math.Round(Izz / 10000, 10)
            }


        curLink.momentOfInertia = momOfInert

        'Send the Component occurence to the link for STL file creation later
        curLink.compOccur = oCompOccur

        'get the actual file name of the link... not just the component name (gets rid of the :1 :2)
        Dim lFileName As String = oCompOccur.Definition.Document.DisplayName
        curLink.fileName = lFileName

        'get the links RPY in respect to the origin.
        Dim lRPY As ArrayList
        lRPY = LinkRPY(oCompOccur.Transformation)
        curLink.originRPY = lRPY

        'get the links XYZ with respect to the origin
        Dim lxyz As New ArrayList

        lxyz.Add(oCompOccur.Transformation.Cell(4, 1))
        lxyz.Add(oCompOccur.Transformation.Cell(4, 2))
        lxyz.Add(oCompOccur.Transformation.Cell(4, 3))

        curLink.originXYZ = lxyz


        'Add link to combobox            
        LinkComboBox.Items.Add(curLink.Name)

        'add the current link to the robot
        robotObj.AddLink(curLink)
        Return 0
    End Function

    Private Function JointSetup(asmJoint As AssemblyJoint) As Object

        Dim jointPar As ComponentOccurrence
        Dim jointChi As ComponentOccurrence
        Dim curJoint As New AsmJoint
        Dim parName As String
        Dim chiName As String
        Dim jointName As String

        'sets the asmJoint name, replace space with _
        jointName = asmJoint.Name.ToString.Replace(":", "_")
        jointName = jointName.Replace(" ", "_")
        curJoint.Name = jointName

        'set the asmJoint Parent and Child
        jointPar = asmJoint.OccurrenceTwo()
        parName = jointPar.Name.ToString.Replace(" ", "_")
        parName = parName.Replace(":", "_")
        curJoint.parentName = parName

        jointChi = asmJoint.OccurrenceOne()
        chiName = jointChi.Name.ToString.Replace(" ", "_")
        chiName = chiName.Replace(":", "_")
        curJoint.childName = chiName

        curJoint.definition = asmJoint.Definition()
        curJoint.jointObject = asmJoint

        'get the parents Link Object
        Dim jointParent As New Link
        For Each pLink As Link In robotObj.Links
            OutPutLine(pLink.Name)
            If pLink.Name = parName Then
                jointParent = pLink
            End If
        Next

        'get the childs Link Object
        Dim jointChild As New Link
        For Each cLink As Link In robotObj.Links
            If cLink.Name = chiName Then
                jointChild = cLink
            End If
        Next

        'get the origin RPY of the joint(origin of the child in respect to the parent)
        Dim jointOrig As New Collection
        Dim oTg As TransientGeometry
        oTg = _invApp.TransientGeometry

        'jointOrigRPY = MatrixRPY(jointChild.transMatrix, jointParent.transMatrix, oTg)
        jointOrig = MatrixBaseChange(jointChild.transMatrix, jointParent.transMatrix, jointParent.Name)
        curJoint.originRPY = jointOrig(1)

        'get the origin XYZ of the joint(origin of the child in respect to the parent)
        curJoint.originXYZ = jointOrig(2)

        'get the current joint type
        Dim jType As String = asmJoint.Definition.JointType
        'Dim llimit As Object
        'Dim ulimit As Object
        'Dim typ As Type


        If jType = 102402 Then
            If asmJoint.Definition.HasAngularPositionLimits = False Then
                curJoint.Type = "continuous"
            Else
                'ulimit = asmJoint.Definition.AngularPositionEndLimit
                'llimit = asmJoint.Definition.AngularPositionStartLimit
                'curJoint.UpperLimit = ulimit.ModelValue.ToString
                'curJoint.LowerLimit = llimit.ModelValue.ToString
                'OutPutLine("Lower=" & Microsoft.VisualBasic.Information.TypeName(llimit) & "  Upper=" & Microsoft.VisualBasic.Information.TypeName(ulimit))
                curJoint.IsReady = False
                curJoint.Type = "revolute"
            End If

        ElseIf jType = 102401 Then
            curJoint.Type = "rigid"
        End If

        'set the default effort and velocity limits to 100
        curJoint.EffortLimit = 100
        curJoint.VelocityLimit = 100

        'add current joint to the joint combobox
        JointComboBox.Items.Add(curJoint.Name)

        robotObj.AddJoint(curJoint)
        Return 0
    End Function

    Private Function writeCommandOutput() As Object
        ' Get the CommandManager object. 
        Dim oCommandMgr As CommandManager = _invApp.CommandManager

        ' Get the collection of control definitions. 
        Dim oControlDefs As ControlDefinitions = oCommandMgr.ControlDefinitions

        Dim stw As System.IO.StreamWriter

        ' Open the file and print out a header line. 
        Dim oControlDef As ControlDefinition
        stw = My.Computer.FileSystem.OpenTextFileWriter("C:\temp\CommandNames.txt", False)
        stw.AutoFlush = True

        ' Iterate through the controls and write out the name. 
        For Each oControlDef In oControlDefs
            stw.WriteLine(oControlDef.InternalName & "     " & oControlDef.DescriptionText)
            stw.WriteLine(" ")
        Next

        ' Close the file. 
        stw.Close()

        Return 0

    End Function
End Class
