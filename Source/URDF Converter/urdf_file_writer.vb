﻿Imports System.IO

Public Class URDF_File_Writer
    'get the tab counter set up for writing file
    Dim tabCount As New TabCounter With {
            .Count = 0
        }


    Public Function WriteFile(projectPath As String, projectName As String, robotObj As Robot) As Object

        'open the actual urdf file
        Dim oFile As System.IO.StreamWriter
        oFile = OpenFile(projectPath, robotObj.Name)

        Call WriteHeader(oFile, robotObj)

        Call WriteMaterial(oFile, robotObj)

        For Each link As Link In robotObj.Links

            Call WriteLink(oFile, link, projectName, projectPath)

        Next

        For Each joint As AsmJoint In robotObj.Joints

            Call WriteJoint(oFile, joint)

        Next

        tabCount.SubTab()
        oFile.WriteLine("</robot>")

        Return 0
    End Function

    'opens the urdf file for editing
    Private Function OpenFile(projectPath As String, projectName As String) As StreamWriter
        Dim newFile As System.IO.StreamWriter
        Dim completeFile As String = projectPath & "urdf\" & projectName & ".urdf"

        'check to see if the directory exists and if not creates one
        If My.Computer.FileSystem.DirectoryExists(projectPath & "urdf\") = False Then
            My.Computer.FileSystem.CreateDirectory(projectPath & "urdf\")
            newFile = My.Computer.FileSystem.OpenTextFileWriter(completeFile, True)
        Else
            'checks to see if the file already exists and deletes it if it does
            If My.Computer.FileSystem.FileExists(completeFile) = True Then
                System.Diagnostics.Debug.WriteLine("FILE ALREADY EXISTED!!!!")
                My.Computer.FileSystem.DeleteFile(completeFile)
            End If

            newFile = My.Computer.FileSystem.OpenTextFileWriter(completeFile, True)
        End If

        newFile.AutoFlush = True

        System.Diagnostics.Debug.WriteLine("done writing file")

        Return newFile

    End Function

    Private Function WriteHeader(swFile As StreamWriter, robObj As Robot) As Object
        'writing a single line to the open file------- TO USE " YOU HAVE TO PUT A " IN FRONT OF IT
        swFile.WriteLine("<?xml version=""2.0""?>")

        'prepare robot name string
        Dim robotNameElement As String
        robotNameElement = "<robot name=""" & robObj.Name & """>"

        'write robot name to file
        swFile.WriteLine(robotNameElement)
        'add a blank line for readability
        swFile.WriteLine(" ")

        'add a tab after robot name
        tabCount.AddTab()

        Return 0

    End Function

    Private Function WriteLink(swFile As StreamWriter, link As Link, packageName As String, packagePath As String) As Object
        'use this integer to send to the functions
        Dim curTabCount As Integer
        'this will be the string that is written to the file
        Dim linkNameString As String

        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        'starting the link element
        'this will be used for converting the link name to XML format
        Dim linkNameXML As String

        'add the XML formatting to the linkName
        linkNameXML = "<link name=""" & link.Name & """>"

        'add the appropriate amount of tabs before the string
        linkNameString = addTabs(linkNameXML, tabCount.Count)

        'write the first line of the link element 
        swFile.WriteLine(linkNameString)
        'tab over after writing the first line of the link
        tabCount.AddTab()
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''




        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        'write inertial part of link

        Dim inertailOpen As String = "<inertial>"
        Dim inertailClose As String = "</inertial>"

        'write the open to the link inertial
        curTabCount = tabCount.Count
        inertailOpen = addTabs(inertailOpen, curTabCount)
        swFile.WriteLine(inertailOpen)

        'tab over after inertial is open
        tabCount.AddTab()

        'write the inertial origin
        'the inertial origin needs to be at the center of mass
        Dim inOrig As String = "<origin rpy=""0 0 0"" xyz=""" & link.centerOfMass(0) & " " & link.centerOfMass(1) & " " & link.centerOfMass(2) & """ />"
        curTabCount = tabCount.Count
        inOrig = addTabs(inOrig, curTabCount)
        swFile.WriteLine(inOrig)

        'write the mass portion of inertia
        Dim inMass As String = "<mass value=""" & link.mass & """/>"
        curTabCount = tabCount.Count
        inMass = addTabs(inMass, curTabCount)
        swFile.WriteLine(inMass)

        'write the inertia portion of inertial
        Dim inertiaString As String = "<inertia"
        inertiaString = inertiaString & " ixx=""" & link.momentOfInertia(0) & """"
        inertiaString = inertiaString & " ixy=""" & link.momentOfInertia(1) & """"
        inertiaString = inertiaString & " ixz=""" & link.momentOfInertia(2) & """"
        inertiaString = inertiaString & " iyy=""" & link.momentOfInertia(3) & """"
        inertiaString = inertiaString & " iyz=""" & link.momentOfInertia(4) & """"
        inertiaString = inertiaString & " izz=""" & link.momentOfInertia(5) & """"
        inertiaString = inertiaString & " />"
        curTabCount = tabCount.Count
        inertiaString = addTabs(inertiaString, curTabCount)
        swFile.WriteLine(inertiaString)

        'tab back over after inertia
        tabCount.SubTab()

        'write the close to the link inertial
        curTabCount = tabCount.Count
        inertailClose = addTabs(inertailClose, curTabCount)
        swFile.WriteLine(inertailClose)

        'write a blank line for readability
        swFile.WriteLine(" ")

        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        'write visual part of link
        Dim visOpen As String = "<visual>"
        Dim visClose As String = "</visual>"
        Dim geoOpen As String = "<geometry>"
        Dim geoClose As String = "</geometry>"


        'write visual open line
        curTabCount = tabCount.Count
        visOpen = addTabs(visOpen, curTabCount)
        swFile.WriteLine(visOpen)

        'tab over after visual is open
        tabCount.AddTab()

        'write the geometry portion of visual
        curTabCount = tabCount.Count
        geoOpen = addTabs(geoOpen, curTabCount)
        swFile.WriteLine(geoOpen)

        'tab over after geometry is open
        tabCount.AddTab()

        'write the mesh portion of geometry
        'package://NAME_OF_PACKAGE/path
        Dim fileString As String
        If InStr(link.fileName, ".") > 0 Then
            fileString = link.fileName.Substring(0, InStr(link.fileName, ".") - 1)
        Else
            fileString = link.fileName
        End If

        Dim curPathToMesh As String = "package://" & packageName & "/meshes/" & fileString.Replace(" ", "_") & ".stl"
        Dim meshString As String = "<mesh filename=""" & curPathToMesh & """/>"
        curTabCount = tabCount.Count
        meshString = addTabs(meshString, curTabCount)
        swFile.WriteLine(meshString)


        'tab back after writing mesh
        tabCount.SubTab()

        'close the geometry section
        curTabCount = tabCount.Count
        geoClose = addTabs(geoClose, curTabCount)
        swFile.WriteLine(geoClose)

        'write the material line
        Dim matLine As String
        matLine = "<material name=""" & link.Material.Name & """/>"
        swFile.WriteLine(addTabs(matLine, tabCount.Count))

        'tab back for close of visual
        tabCount.SubTab()

        'write visual close line
        curTabCount = tabCount.Count
        visClose = addTabs(visClose, curTabCount)
        swFile.WriteLine(visClose)

        'add a blank line under visual for readability
        swFile.WriteLine(" ")

        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        'write collision part of link
        Dim colOpen As String = "<collision>"
        Dim colClose As String = "</collision>"

        'write collision open line
        curTabCount = tabCount.Count
        colOpen = addTabs(colOpen, curTabCount)
        swFile.WriteLine(colOpen)

        'tab over after opening collision
        tabCount.AddTab()

        'write the geometry portion of visual
        curTabCount = tabCount.Count
        'dont have to tab over because we are re-using the string
        'geoOpen = addTabs(geoOpen, curTabCount)
        swFile.WriteLine(geoOpen)

        'tab over after geometry is open
        tabCount.AddTab()

        'write the mesh portion of geometry
        curTabCount = tabCount.Count
        'dont have to tab over because we are re-using the string
        'meshString = addTabs(meshString, curTabCount)
        swFile.WriteLine(meshString)

        'tab back after writing mesh
        tabCount.SubTab()

        'close the geometry section
        curTabCount = tabCount.Count
        'dont have to tab over because we are re-using the string
        'geoClose = addTabs(geoClose, curTabCount)
        swFile.WriteLine(geoClose)

        'tab back to close collision
        tabCount.SubTab()

        'write collision close line
        curTabCount = tabCount.Count
        colClose = addTabs(colClose, curTabCount)
        swFile.WriteLine(colClose)

        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        'write the close of the link element
        tabCount.SubTab()
        swFile.WriteLine(addTabs("</link>", tabCount.Count))
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        'write 2 blank lines after link for readability
        swFile.WriteLine(" ")
        swFile.WriteLine(" ")

        Return 0
    End Function

    Private Function WriteJoint(swFile As StreamWriter, joint As AsmJoint) As Object

        'write the beginning of the joint

        swFile.WriteLine(addTabs("<joint name=""" & joint.Name & """ type=""" & joint.Type & """>", tabCount.Count))


        'tab over and write the parent link 
        tabCount.AddTab()
        swFile.WriteLine(addTabs("<parent link=""" & joint.parentName & """/>", tabCount.Count))

        'write the chile link 
        swFile.WriteLine(addTabs("<child link=""" & joint.childName & """/>", tabCount.Count))

        'write the joint origin
        Dim jOrigin As String = "<origin xyz="""
        jOrigin = jOrigin & joint.originXYZ(0) & " " & joint.originXYZ(1) & " " & joint.originXYZ(2) & """"
        jOrigin = jOrigin & " rpy="""
        jOrigin = jOrigin & joint.originRPY(0) & " " & joint.originRPY(1) & " " & joint.originRPY(2) & """"
        jOrigin = jOrigin & "/>"

        swFile.WriteLine(addTabs(jOrigin, tabCount.Count))

        'write joint limits if applicable
        If joint.Type = "revolute" Then
            Dim limString As String
            'limString = "<limit lower=""" & joint.LowerLimit & """ upper=""" & joint.UpperLimit & """/>"
            limString = "<limit effort=""" & joint.EffortLimit & """ velocity=""" & joint.VelocityLimit & """"
            limString = limString & " lower=""" & joint.LowerLimit & """ upper=""" & joint.UpperLimit & """/>"
            swFile.WriteLine(addTabs(limString, tabCount.Count))
        End If

        'write joint axis
        swFile.WriteLine(addTabs("<axis xyz=""0 0 1""/>", tabCount.Count))



        'tab back and end the joint
        tabCount.SubTab()
        swFile.WriteLine(addTabs("</joint>", tabCount.Count))

        'add 2 blank lines for readability
        swFile.WriteLine(" ")
        swFile.WriteLine(" ")

        Return 0
    End Function

    Private Function addTabs(inString As String, tabCount As Integer) As String
        Dim outString As String = ""
        Dim x As Integer = 0

        If tabCount = 0 Then
            outString = inString
            Return outString
        End If

        While x < tabCount
            outString = outString & "   "
            x += 1
        End While

        outString = outString & inString

        Return outString

    End Function

    Private Function WriteMaterial(swFile As StreamWriter, roboObj As Robot) As Object
        Dim curString As String

        For Each mat As LinkMaterial In roboObj.MaterialsUsed
            curString = "<material name=""" & mat.Name & """>"
            swFile.WriteLine(addTabs(curString, tabCount.Count))

            tabCount.AddTab()

            curString = "<color rgba=""" & mat.Red & " " & mat.Green & " " & mat.Blue & " " & mat.Alpha & """/>"
            swFile.WriteLine(addTabs(curString, tabCount.Count))

            tabCount.SubTab()

            swFile.WriteLine(addTabs("</material>", tabCount.Count))

            swFile.WriteLine(" ")
        Next


        Return 0
    End Function

End Class
