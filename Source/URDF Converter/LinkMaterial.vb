﻿Imports System.Math

Public Class LinkMaterial

    Dim mName As String
    Dim rgbVal As ArrayList
    Dim r, g, b, a As Double

    Property Name As String
        Get
            Return mName
        End Get
        Set(value As String)
            mName = value
        End Set
    End Property

    Property Red As Double
        Get
            Return r
        End Get
        Set(value As Double)
            r = Math.Round(value, 4)
        End Set
    End Property

    Property Green As Double
        Get
            Return g
        End Get
        Set(value As Double)
            g = Math.Round(value, 4)
        End Set
    End Property

    Property Blue As Double
        Get
            Return b
        End Get
        Set(value As Double)
            b = Math.Round(value, 4)
        End Set
    End Property

    Property Alpha As Double
        Get
            Return a
        End Get
        Set(value As Double)
            a = Math.Round(value, 4)
        End Set
    End Property
End Class
