﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form1))
        Me.jointParentLabel = New System.Windows.Forms.Label()
        Me.jointChildLabel = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.LinkNameTextBox = New System.Windows.Forms.TextBox()
        Me.LinkMassTextBox = New System.Windows.Forms.TextBox()
        Me.createButton = New System.Windows.Forms.Button()
        Me.FolderBrowserDialog1 = New System.Windows.Forms.FolderBrowserDialog()
        Me.ProjectFolderLabel = New System.Windows.Forms.Label()
        Me.ProjectDirectoryTextBox = New System.Windows.Forms.TextBox()
        Me.ProjectDirectoryBrowseButton = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.robotNameTextBox = New System.Windows.Forms.TextBox()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.FilePage = New System.Windows.Forms.TabPage()
        Me.ROSPackNameTextBox = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.LinkPage = New System.Windows.Forms.TabPage()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.MaterialComboBox = New System.Windows.Forms.ComboBox()
        Me.LinkCancelButton = New System.Windows.Forms.Button()
        Me.LinkApplyButton = New System.Windows.Forms.Button()
        Me.LinkComboBox = New System.Windows.Forms.ComboBox()
        Me.JointPage = New System.Windows.Forms.TabPage()
        Me.EffortLimitTextBox = New System.Windows.Forms.TextBox()
        Me.VelocityLimitTextBox = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.JointCancelButton = New System.Windows.Forms.Button()
        Me.JointApplyButton = New System.Windows.Forms.Button()
        Me.LowerLimitTextBox = New System.Windows.Forms.TextBox()
        Me.UpperLimitTextBox = New System.Windows.Forms.TextBox()
        Me.Label40 = New System.Windows.Forms.Label()
        Me.Label32 = New System.Windows.Forms.Label()
        Me.JointTypeLabel = New System.Windows.Forms.Label()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.JointComboBox = New System.Windows.Forms.ComboBox()
        Me.MaterialTab = New System.Windows.Forms.TabPage()
        Me.ColorRectangle = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.MaterialEditButton = New System.Windows.Forms.Button()
        Me.MaterialCreateButton = New System.Windows.Forms.Button()
        Me.MaterialNameTextBox = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.MaterialCancelButton = New System.Windows.Forms.Button()
        Me.MaterialApplyButton = New System.Windows.Forms.Button()
        Me.MaterialComboBox2 = New System.Windows.Forms.ComboBox()
        Me.ColorDialog1 = New System.Windows.Forms.ColorDialog()
        Me.TabControl1.SuspendLayout()
        Me.FilePage.SuspendLayout()
        Me.LinkPage.SuspendLayout()
        Me.JointPage.SuspendLayout()
        Me.MaterialTab.SuspendLayout()
        Me.SuspendLayout()
        '
        'jointParentLabel
        '
        Me.jointParentLabel.AutoSize = True
        Me.jointParentLabel.ForeColor = System.Drawing.Color.White
        Me.jointParentLabel.Location = New System.Drawing.Point(125, 67)
        Me.jointParentLabel.Name = "jointParentLabel"
        Me.jointParentLabel.Size = New System.Drawing.Size(64, 21)
        Me.jointParentLabel.TabIndex = 3
        Me.jointParentLabel.Text = "Label2"
        '
        'jointChildLabel
        '
        Me.jointChildLabel.AutoSize = True
        Me.jointChildLabel.ForeColor = System.Drawing.Color.White
        Me.jointChildLabel.Location = New System.Drawing.Point(125, 103)
        Me.jointChildLabel.Name = "jointChildLabel"
        Me.jointChildLabel.Size = New System.Drawing.Size(64, 21)
        Me.jointChildLabel.TabIndex = 4
        Me.jointChildLabel.Text = "Label3"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("SansSerif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(41, 92)
        Me.Label11.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(58, 21)
        Me.Label11.TabIndex = 12
        Me.Label11.Text = "Mass:"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("SansSerif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(4, 58)
        Me.Label12.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(100, 21)
        Me.Label12.TabIndex = 13
        Me.Label12.Text = "Link Name:"
        '
        'LinkNameTextBox
        '
        Me.LinkNameTextBox.Location = New System.Drawing.Point(108, 55)
        Me.LinkNameTextBox.Margin = New System.Windows.Forms.Padding(2)
        Me.LinkNameTextBox.Name = "LinkNameTextBox"
        Me.LinkNameTextBox.Size = New System.Drawing.Size(192, 28)
        Me.LinkNameTextBox.TabIndex = 14
        '
        'LinkMassTextBox
        '
        Me.LinkMassTextBox.Location = New System.Drawing.Point(108, 89)
        Me.LinkMassTextBox.Margin = New System.Windows.Forms.Padding(2)
        Me.LinkMassTextBox.Name = "LinkMassTextBox"
        Me.LinkMassTextBox.Size = New System.Drawing.Size(192, 28)
        Me.LinkMassTextBox.TabIndex = 15
        '
        'createButton
        '
        Me.createButton.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.createButton.Location = New System.Drawing.Point(224, 192)
        Me.createButton.Name = "createButton"
        Me.createButton.Size = New System.Drawing.Size(140, 36)
        Me.createButton.TabIndex = 64
        Me.createButton.Text = "Create Files"
        Me.createButton.UseVisualStyleBackColor = True
        '
        'ProjectFolderLabel
        '
        Me.ProjectFolderLabel.AutoSize = True
        Me.ProjectFolderLabel.Location = New System.Drawing.Point(3, 37)
        Me.ProjectFolderLabel.Name = "ProjectFolderLabel"
        Me.ProjectFolderLabel.Size = New System.Drawing.Size(148, 21)
        Me.ProjectFolderLabel.TabIndex = 65
        Me.ProjectFolderLabel.Text = "Project Directory:"
        '
        'ProjectDirectoryTextBox
        '
        Me.ProjectDirectoryTextBox.Location = New System.Drawing.Point(161, 34)
        Me.ProjectDirectoryTextBox.Name = "ProjectDirectoryTextBox"
        Me.ProjectDirectoryTextBox.Size = New System.Drawing.Size(432, 28)
        Me.ProjectDirectoryTextBox.TabIndex = 66
        '
        'ProjectDirectoryBrowseButton
        '
        Me.ProjectDirectoryBrowseButton.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.ProjectDirectoryBrowseButton.Location = New System.Drawing.Point(455, 66)
        Me.ProjectDirectoryBrowseButton.Name = "ProjectDirectoryBrowseButton"
        Me.ProjectDirectoryBrowseButton.Size = New System.Drawing.Size(138, 33)
        Me.ProjectDirectoryBrowseButton.TabIndex = 67
        Me.ProjectDirectoryBrowseButton.Text = "Browse"
        Me.ProjectDirectoryBrowseButton.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(69, 108)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(115, 21)
        Me.Label1.TabIndex = 68
        Me.Label1.Text = "Robot Name:"
        '
        'robotNameTextBox
        '
        Me.robotNameTextBox.Location = New System.Drawing.Point(196, 105)
        Me.robotNameTextBox.Name = "robotNameTextBox"
        Me.robotNameTextBox.Size = New System.Drawing.Size(397, 28)
        Me.robotNameTextBox.TabIndex = 69
        Me.robotNameTextBox.Text = "DEFAULT_PACKAGE_NAME"
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.FilePage)
        Me.TabControl1.Controls.Add(Me.LinkPage)
        Me.TabControl1.Controls.Add(Me.JointPage)
        Me.TabControl1.Controls.Add(Me.MaterialTab)
        Me.TabControl1.Font = New System.Drawing.Font("SansSerif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TabControl1.Location = New System.Drawing.Point(1, 0)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(607, 266)
        Me.TabControl1.TabIndex = 70
        '
        'FilePage
        '
        Me.FilePage.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.FilePage.Controls.Add(Me.ROSPackNameTextBox)
        Me.FilePage.Controls.Add(Me.Label9)
        Me.FilePage.Controls.Add(Me.ProjectDirectoryBrowseButton)
        Me.FilePage.Controls.Add(Me.createButton)
        Me.FilePage.Controls.Add(Me.Label1)
        Me.FilePage.Controls.Add(Me.robotNameTextBox)
        Me.FilePage.Controls.Add(Me.ProjectDirectoryTextBox)
        Me.FilePage.Controls.Add(Me.ProjectFolderLabel)
        Me.FilePage.ForeColor = System.Drawing.Color.White
        Me.FilePage.Location = New System.Drawing.Point(4, 30)
        Me.FilePage.Name = "FilePage"
        Me.FilePage.Padding = New System.Windows.Forms.Padding(3)
        Me.FilePage.Size = New System.Drawing.Size(599, 232)
        Me.FilePage.TabIndex = 0
        Me.FilePage.Text = "File"
        '
        'ROSPackNameTextBox
        '
        Me.ROSPackNameTextBox.Cursor = System.Windows.Forms.Cursors.SizeAll
        Me.ROSPackNameTextBox.Location = New System.Drawing.Point(196, 148)
        Me.ROSPackNameTextBox.Name = "ROSPackNameTextBox"
        Me.ROSPackNameTextBox.Size = New System.Drawing.Size(397, 28)
        Me.ROSPackNameTextBox.TabIndex = 71
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(3, 151)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(181, 21)
        Me.Label9.TabIndex = 70
        Me.Label9.Text = "ROS Package Name:"
        '
        'LinkPage
        '
        Me.LinkPage.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.LinkPage.Controls.Add(Me.Label2)
        Me.LinkPage.Controls.Add(Me.MaterialComboBox)
        Me.LinkPage.Controls.Add(Me.LinkCancelButton)
        Me.LinkPage.Controls.Add(Me.LinkApplyButton)
        Me.LinkPage.Controls.Add(Me.LinkComboBox)
        Me.LinkPage.Controls.Add(Me.LinkNameTextBox)
        Me.LinkPage.Controls.Add(Me.Label12)
        Me.LinkPage.Controls.Add(Me.LinkMassTextBox)
        Me.LinkPage.Controls.Add(Me.Label11)
        Me.LinkPage.ForeColor = System.Drawing.Color.White
        Me.LinkPage.Location = New System.Drawing.Point(4, 30)
        Me.LinkPage.Name = "LinkPage"
        Me.LinkPage.Padding = New System.Windows.Forms.Padding(3)
        Me.LinkPage.Size = New System.Drawing.Size(599, 232)
        Me.LinkPage.TabIndex = 1
        Me.LinkPage.Text = "Links"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("SansSerif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(25, 127)
        Me.Label2.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(79, 21)
        Me.Label2.TabIndex = 20
        Me.Label2.Text = "Material:"
        '
        'MaterialComboBox
        '
        Me.MaterialComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.MaterialComboBox.FormattingEnabled = True
        Me.MaterialComboBox.Location = New System.Drawing.Point(109, 124)
        Me.MaterialComboBox.Name = "MaterialComboBox"
        Me.MaterialComboBox.Size = New System.Drawing.Size(192, 29)
        Me.MaterialComboBox.TabIndex = 19
        '
        'LinkCancelButton
        '
        Me.LinkCancelButton.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.LinkCancelButton.Location = New System.Drawing.Point(306, 186)
        Me.LinkCancelButton.Name = "LinkCancelButton"
        Me.LinkCancelButton.Size = New System.Drawing.Size(126, 29)
        Me.LinkCancelButton.TabIndex = 16
        Me.LinkCancelButton.Text = "Cancel"
        Me.LinkCancelButton.UseVisualStyleBackColor = True
        '
        'LinkApplyButton
        '
        Me.LinkApplyButton.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.LinkApplyButton.Location = New System.Drawing.Point(174, 186)
        Me.LinkApplyButton.Name = "LinkApplyButton"
        Me.LinkApplyButton.Size = New System.Drawing.Size(126, 29)
        Me.LinkApplyButton.TabIndex = 15
        Me.LinkApplyButton.Text = "Apply"
        Me.LinkApplyButton.UseVisualStyleBackColor = True
        '
        'LinkComboBox
        '
        Me.LinkComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.LinkComboBox.FormattingEnabled = True
        Me.LinkComboBox.Location = New System.Drawing.Point(15, 18)
        Me.LinkComboBox.Name = "LinkComboBox"
        Me.LinkComboBox.Size = New System.Drawing.Size(284, 29)
        Me.LinkComboBox.TabIndex = 0
        '
        'JointPage
        '
        Me.JointPage.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.JointPage.Controls.Add(Me.EffortLimitTextBox)
        Me.JointPage.Controls.Add(Me.VelocityLimitTextBox)
        Me.JointPage.Controls.Add(Me.Label8)
        Me.JointPage.Controls.Add(Me.Label7)
        Me.JointPage.Controls.Add(Me.JointCancelButton)
        Me.JointPage.Controls.Add(Me.JointApplyButton)
        Me.JointPage.Controls.Add(Me.LowerLimitTextBox)
        Me.JointPage.Controls.Add(Me.UpperLimitTextBox)
        Me.JointPage.Controls.Add(Me.Label40)
        Me.JointPage.Controls.Add(Me.Label32)
        Me.JointPage.Controls.Add(Me.JointTypeLabel)
        Me.JointPage.Controls.Add(Me.Label28)
        Me.JointPage.Controls.Add(Me.Label26)
        Me.JointPage.Controls.Add(Me.Label3)
        Me.JointPage.Controls.Add(Me.JointComboBox)
        Me.JointPage.Controls.Add(Me.jointParentLabel)
        Me.JointPage.Controls.Add(Me.jointChildLabel)
        Me.JointPage.ForeColor = System.Drawing.Color.White
        Me.JointPage.Location = New System.Drawing.Point(4, 30)
        Me.JointPage.Name = "JointPage"
        Me.JointPage.Padding = New System.Windows.Forms.Padding(3)
        Me.JointPage.Size = New System.Drawing.Size(599, 232)
        Me.JointPage.TabIndex = 2
        Me.JointPage.Text = "Joints"
        '
        'EffortLimitTextBox
        '
        Me.EffortLimitTextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.EffortLimitTextBox.Location = New System.Drawing.Point(404, 137)
        Me.EffortLimitTextBox.Name = "EffortLimitTextBox"
        Me.EffortLimitTextBox.Size = New System.Drawing.Size(175, 28)
        Me.EffortLimitTextBox.TabIndex = 33
        '
        'VelocityLimitTextBox
        '
        Me.VelocityLimitTextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.VelocityLimitTextBox.Location = New System.Drawing.Point(404, 100)
        Me.VelocityLimitTextBox.Name = "VelocityLimitTextBox"
        Me.VelocityLimitTextBox.Size = New System.Drawing.Size(175, 28)
        Me.VelocityLimitTextBox.TabIndex = 32
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(298, 140)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(100, 21)
        Me.Label8.TabIndex = 31
        Me.Label8.Text = "Effort Limit:"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(281, 103)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(118, 21)
        Me.Label7.TabIndex = 30
        Me.Label7.Text = "Velocity Limit:"
        '
        'JointCancelButton
        '
        Me.JointCancelButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.JointCancelButton.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.JointCancelButton.ForeColor = System.Drawing.SystemColors.Control
        Me.JointCancelButton.Location = New System.Drawing.Point(296, 188)
        Me.JointCancelButton.Name = "JointCancelButton"
        Me.JointCancelButton.Size = New System.Drawing.Size(126, 29)
        Me.JointCancelButton.TabIndex = 29
        Me.JointCancelButton.Text = "Cancel"
        Me.JointCancelButton.UseVisualStyleBackColor = False
        '
        'JointApplyButton
        '
        Me.JointApplyButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.JointApplyButton.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.JointApplyButton.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.JointApplyButton.ForeColor = System.Drawing.SystemColors.Control
        Me.JointApplyButton.Location = New System.Drawing.Point(146, 188)
        Me.JointApplyButton.Name = "JointApplyButton"
        Me.JointApplyButton.Size = New System.Drawing.Size(126, 29)
        Me.JointApplyButton.TabIndex = 28
        Me.JointApplyButton.Text = "Apply"
        Me.JointApplyButton.UseVisualStyleBackColor = False
        '
        'LowerLimitTextBox
        '
        Me.LowerLimitTextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.LowerLimitTextBox.Location = New System.Drawing.Point(404, 64)
        Me.LowerLimitTextBox.Name = "LowerLimitTextBox"
        Me.LowerLimitTextBox.Size = New System.Drawing.Size(175, 28)
        Me.LowerLimitTextBox.TabIndex = 27
        '
        'UpperLimitTextBox
        '
        Me.UpperLimitTextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.UpperLimitTextBox.Location = New System.Drawing.Point(404, 27)
        Me.UpperLimitTextBox.Name = "UpperLimitTextBox"
        Me.UpperLimitTextBox.Size = New System.Drawing.Size(175, 28)
        Me.UpperLimitTextBox.TabIndex = 26
        '
        'Label40
        '
        Me.Label40.AutoSize = True
        Me.Label40.Location = New System.Drawing.Point(292, 67)
        Me.Label40.Name = "Label40"
        Me.Label40.Size = New System.Drawing.Size(106, 21)
        Me.Label40.TabIndex = 25
        Me.Label40.Text = "Lower Limit:"
        '
        'Label32
        '
        Me.Label32.AutoSize = True
        Me.Label32.Location = New System.Drawing.Point(293, 30)
        Me.Label32.Name = "Label32"
        Me.Label32.Size = New System.Drawing.Size(106, 21)
        Me.Label32.TabIndex = 24
        Me.Label32.Text = "Upper Limit:"
        '
        'JointTypeLabel
        '
        Me.JointTypeLabel.AutoSize = True
        Me.JointTypeLabel.ForeColor = System.Drawing.Color.White
        Me.JointTypeLabel.Location = New System.Drawing.Point(125, 140)
        Me.JointTypeLabel.Name = "JointTypeLabel"
        Me.JointTypeLabel.Size = New System.Drawing.Size(64, 21)
        Me.JointTypeLabel.TabIndex = 23
        Me.JointTypeLabel.Text = "Label3"
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        Me.Label28.Location = New System.Drawing.Point(22, 140)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(98, 21)
        Me.Label28.TabIndex = 22
        Me.Label28.Text = "Joint Type:"
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.Location = New System.Drawing.Point(23, 103)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(94, 21)
        Me.Label26.TabIndex = 21
        Me.Label26.Text = "Child Link:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(13, 67)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(106, 21)
        Me.Label3.TabIndex = 20
        Me.Label3.Text = "Parent Link:"
        '
        'JointComboBox
        '
        Me.JointComboBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.JointComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.JointComboBox.FormattingEnabled = True
        Me.JointComboBox.Location = New System.Drawing.Point(16, 17)
        Me.JointComboBox.Name = "JointComboBox"
        Me.JointComboBox.Size = New System.Drawing.Size(256, 29)
        Me.JointComboBox.TabIndex = 1
        '
        'MaterialTab
        '
        Me.MaterialTab.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.MaterialTab.Controls.Add(Me.ColorRectangle)
        Me.MaterialTab.Controls.Add(Me.Label6)
        Me.MaterialTab.Controls.Add(Me.MaterialEditButton)
        Me.MaterialTab.Controls.Add(Me.MaterialCreateButton)
        Me.MaterialTab.Controls.Add(Me.MaterialNameTextBox)
        Me.MaterialTab.Controls.Add(Me.Label4)
        Me.MaterialTab.Controls.Add(Me.Label5)
        Me.MaterialTab.Controls.Add(Me.MaterialCancelButton)
        Me.MaterialTab.Controls.Add(Me.MaterialApplyButton)
        Me.MaterialTab.Controls.Add(Me.MaterialComboBox2)
        Me.MaterialTab.Location = New System.Drawing.Point(4, 30)
        Me.MaterialTab.Name = "MaterialTab"
        Me.MaterialTab.Padding = New System.Windows.Forms.Padding(3)
        Me.MaterialTab.Size = New System.Drawing.Size(599, 232)
        Me.MaterialTab.TabIndex = 3
        Me.MaterialTab.Text = "Material"
        '
        'ColorRectangle
        '
        Me.ColorRectangle.AutoSize = True
        Me.ColorRectangle.BackColor = System.Drawing.Color.LightGray
        Me.ColorRectangle.ForeColor = System.Drawing.SystemColors.ActiveBorder
        Me.ColorRectangle.Location = New System.Drawing.Point(142, 101)
        Me.ColorRectangle.Name = "ColorRectangle"
        Me.ColorRectangle.Size = New System.Drawing.Size(160, 21)
        Me.ColorRectangle.TabIndex = 31
        Me.ColorRectangle.Text = "                              "
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.BackColor = System.Drawing.Color.Maroon
        Me.Label6.Font = New System.Drawing.Font("SansSerif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(141, 100)
        Me.Label6.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(0, 21)
        Me.Label6.TabIndex = 30
        '
        'MaterialEditButton
        '
        Me.MaterialEditButton.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.MaterialEditButton.Location = New System.Drawing.Point(440, 21)
        Me.MaterialEditButton.Name = "MaterialEditButton"
        Me.MaterialEditButton.Size = New System.Drawing.Size(126, 29)
        Me.MaterialEditButton.TabIndex = 28
        Me.MaterialEditButton.Text = "Edit"
        Me.MaterialEditButton.UseVisualStyleBackColor = True
        '
        'MaterialCreateButton
        '
        Me.MaterialCreateButton.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.MaterialCreateButton.Location = New System.Drawing.Point(308, 21)
        Me.MaterialCreateButton.Name = "MaterialCreateButton"
        Me.MaterialCreateButton.Size = New System.Drawing.Size(126, 29)
        Me.MaterialCreateButton.TabIndex = 27
        Me.MaterialCreateButton.Text = "Create"
        Me.MaterialCreateButton.UseVisualStyleBackColor = True
        '
        'MaterialNameTextBox
        '
        Me.MaterialNameTextBox.Location = New System.Drawing.Point(141, 65)
        Me.MaterialNameTextBox.Margin = New System.Windows.Forms.Padding(2)
        Me.MaterialNameTextBox.Name = "MaterialNameTextBox"
        Me.MaterialNameTextBox.Size = New System.Drawing.Size(192, 28)
        Me.MaterialNameTextBox.TabIndex = 25
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("SansSerif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.White
        Me.Label4.Location = New System.Drawing.Point(15, 68)
        Me.Label4.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(131, 21)
        Me.Label4.TabIndex = 24
        Me.Label4.Text = "Material Name:"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("SansSerif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.White
        Me.Label5.Location = New System.Drawing.Point(79, 100)
        Me.Label5.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(58, 21)
        Me.Label5.TabIndex = 23
        Me.Label5.Text = "Color:"
        '
        'MaterialCancelButton
        '
        Me.MaterialCancelButton.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.MaterialCancelButton.Location = New System.Drawing.Point(308, 188)
        Me.MaterialCancelButton.Name = "MaterialCancelButton"
        Me.MaterialCancelButton.Size = New System.Drawing.Size(126, 29)
        Me.MaterialCancelButton.TabIndex = 22
        Me.MaterialCancelButton.Text = "Cancel"
        Me.MaterialCancelButton.UseVisualStyleBackColor = True
        '
        'MaterialApplyButton
        '
        Me.MaterialApplyButton.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.MaterialApplyButton.Location = New System.Drawing.Point(176, 188)
        Me.MaterialApplyButton.Name = "MaterialApplyButton"
        Me.MaterialApplyButton.Size = New System.Drawing.Size(126, 29)
        Me.MaterialApplyButton.TabIndex = 21
        Me.MaterialApplyButton.Text = "Apply"
        Me.MaterialApplyButton.UseVisualStyleBackColor = True
        '
        'MaterialComboBox2
        '
        Me.MaterialComboBox2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.MaterialComboBox2.FormattingEnabled = True
        Me.MaterialComboBox2.Location = New System.Drawing.Point(28, 22)
        Me.MaterialComboBox2.Name = "MaterialComboBox2"
        Me.MaterialComboBox2.Size = New System.Drawing.Size(192, 29)
        Me.MaterialComboBox2.TabIndex = 20
        '
        'ColorDialog1
        '
        Me.ColorDialog1.AnyColor = True
        Me.ColorDialog1.FullOpen = True
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.ClientSize = New System.Drawing.Size(608, 268)
        Me.Controls.Add(Me.TabControl1)
        Me.ForeColor = System.Drawing.Color.White
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "Form1"
        Me.Text = "RoboVerter 1.0.0"
        Me.TabControl1.ResumeLayout(False)
        Me.FilePage.ResumeLayout(False)
        Me.FilePage.PerformLayout()
        Me.LinkPage.ResumeLayout(False)
        Me.LinkPage.PerformLayout()
        Me.JointPage.ResumeLayout(False)
        Me.JointPage.PerformLayout()
        Me.MaterialTab.ResumeLayout(False)
        Me.MaterialTab.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents jointParentLabel As Windows.Forms.Label
    Friend WithEvents jointChildLabel As Windows.Forms.Label
    Friend WithEvents Label11 As Windows.Forms.Label
    Friend WithEvents Label12 As Windows.Forms.Label
    Friend WithEvents LinkNameTextBox As Windows.Forms.TextBox
    Friend WithEvents LinkMassTextBox As Windows.Forms.TextBox
    Friend WithEvents createButton As Windows.Forms.Button
    Friend WithEvents FolderBrowserDialog1 As Windows.Forms.FolderBrowserDialog
    Friend WithEvents ProjectFolderLabel As Windows.Forms.Label
    Friend WithEvents ProjectDirectoryTextBox As Windows.Forms.TextBox
    Friend WithEvents ProjectDirectoryBrowseButton As Windows.Forms.Button
    Friend WithEvents Label1 As Windows.Forms.Label
    Friend WithEvents robotNameTextBox As Windows.Forms.TextBox
    Friend WithEvents TabControl1 As Windows.Forms.TabControl
    Friend WithEvents FilePage As Windows.Forms.TabPage
    Friend WithEvents LinkPage As Windows.Forms.TabPage
    Friend WithEvents JointPage As Windows.Forms.TabPage
    Friend WithEvents Label2 As Windows.Forms.Label
    Friend WithEvents MaterialComboBox As Windows.Forms.ComboBox
    Friend WithEvents LinkCancelButton As Windows.Forms.Button
    Friend WithEvents LinkApplyButton As Windows.Forms.Button
    Friend WithEvents LinkComboBox As Windows.Forms.ComboBox
    Friend WithEvents JointComboBox As Windows.Forms.ComboBox
    Friend WithEvents ColorDialog1 As Windows.Forms.ColorDialog
    Friend WithEvents Label28 As Windows.Forms.Label
    Friend WithEvents Label26 As Windows.Forms.Label
    Friend WithEvents Label3 As Windows.Forms.Label
    Friend WithEvents JointCancelButton As Windows.Forms.Button
    Friend WithEvents JointApplyButton As Windows.Forms.Button
    Friend WithEvents LowerLimitTextBox As Windows.Forms.TextBox
    Friend WithEvents UpperLimitTextBox As Windows.Forms.TextBox
    Friend WithEvents Label40 As Windows.Forms.Label
    Friend WithEvents Label32 As Windows.Forms.Label
    Friend WithEvents JointTypeLabel As Windows.Forms.Label
    Friend WithEvents MaterialTab As Windows.Forms.TabPage
    Friend WithEvents Label6 As Windows.Forms.Label
    Friend WithEvents MaterialEditButton As Windows.Forms.Button
    Friend WithEvents MaterialCreateButton As Windows.Forms.Button
    Friend WithEvents MaterialNameTextBox As Windows.Forms.TextBox
    Friend WithEvents Label4 As Windows.Forms.Label
    Friend WithEvents Label5 As Windows.Forms.Label
    Friend WithEvents MaterialCancelButton As Windows.Forms.Button
    Friend WithEvents MaterialApplyButton As Windows.Forms.Button
    Friend WithEvents MaterialComboBox2 As Windows.Forms.ComboBox
    Friend WithEvents ColorRectangle As Windows.Forms.Label
    Friend WithEvents EffortLimitTextBox As Windows.Forms.TextBox
    Friend WithEvents VelocityLimitTextBox As Windows.Forms.TextBox
    Friend WithEvents Label8 As Windows.Forms.Label
    Friend WithEvents Label7 As Windows.Forms.Label
    Friend WithEvents ROSPackNameTextBox As Windows.Forms.TextBox
    Friend WithEvents Label9 As Windows.Forms.Label
End Class
