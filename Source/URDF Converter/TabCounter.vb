﻿Public Class TabCounter

    Dim tCount As Integer

    Property Count As Integer
        Set(value As Integer)
            tCount = value
        End Set
        Get
            Return tCount
        End Get
    End Property

    Public Function AddTab() As Object
        tCount = tCount + 1
        Return 0
    End Function

    Public Function SubTab() As Object
        tCount = tCount - 1
        Return 0
    End Function

End Class
