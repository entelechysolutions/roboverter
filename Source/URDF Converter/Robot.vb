﻿Imports Inventor
Public Class Robot
    Private robName As String
    Private jointList As New ArrayList
    Private linkList As New ArrayList
    Private origXYZ As ArrayList
    Private origRPY As ArrayList
    Private matInUse As ArrayList

    Property Name() As String
        Get
            Return robName
        End Get
        Set(value As String)
            robName = value
        End Set
    End Property

    Public Function AddJoint(ByVal joint As AsmJoint) As Object
        jointList.Add(joint)
        Return vbNull
    End Function

    Public Function AddLink(ByVal link As Link) As Object
        linkList.Add(link)
        Return vbNull
    End Function

    'returns a list of joints as asmJoints (this is a custom object)
    ReadOnly Property Joints() As ArrayList
        Get
            Return jointList
        End Get
    End Property

    'returns a list of link objects
    ReadOnly Property Links() As ArrayList
        Get
            Return linkList
        End Get
    End Property

    Property originXYZ As ArrayList
        Get
            Return origXYZ
        End Get
        Set(value As ArrayList)
            origXYZ = value
        End Set
    End Property

    Property originRPY As ArrayList
        Get
            Return origRPY
        End Get
        Set(value As ArrayList)
            origRPY = value
        End Set
    End Property

    Property MaterialsUsed As ArrayList
        Get
            Return matInUse
        End Get
        Set(value As ArrayList)
            matInUse = value
        End Set
    End Property

End Class
