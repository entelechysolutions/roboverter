# README #

This repo is a temporary download location until the app gets approve by Autodesk for the app store.

Read the included user guide for details on this programs functionality and how to use it.  

To get Inventor to recognize this addin move RoboVerter.bundle to the following path
**C:\Users\$username$\AppData\Roaming\Autodesk\ApplicationPlugins\RoboVerter.bundle**

Please leave feedback on any issues or potential improvements.